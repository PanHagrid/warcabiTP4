package board;

public class Pawn {
  private int colour; //colour between 0 and 7, 0 means white, unique colour for each player
  private int coordinateX; //information about coordinateX of pawn on board, between 0 and 18
  private int coordinateY; //information about coordinateY of pawn on board, between 0 and 18
  private int id; //pawn id, if empty =-1

  public Pawn(int x, int y, int col) {
    setcolour(col);
    setCoordinateX(x);
    setCooridnateY(y);
    id=-1;
  }
  public Pawn(int x, int y, int col,int id) {
    setcolour(col);
    setCoordinateX(x);
    setCooridnateY(y);
    setID(id);
  }

  //getters and setters
  public void setcolour(int col) {
    colour = col;
  }

  private void setCoordinateX(int x) {
    coordinateX = x;
  }

  private void setCooridnateY(int y) {
    coordinateY = y;
  }
  public void setID(int i) {
    id = i;
  }

  public int getcolour() {
    return colour;
  }

  public int getCoordinateX() {
    return coordinateX;
  }

  public int getCoordinateY() {
    return coordinateY;
  }

  public int getID() {
    return id;
  }
}
