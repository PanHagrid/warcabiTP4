package board;

public class Board {
  public Pawn[][] table;

  public Board() {
    table = new Pawn[17][25];
    int id=0;
    table[0][0]=new Pawn(0,0,0);
    CreateBoard(12, 1, 0,id);
    CreateBoard(11, 2, 1,id);
    CreateBoard(10, 3, 2,id);
    CreateBoard(9, 4, 3,id);
    CreateBoard(0, 13, 4,id);
    CreateBoard(1, 12, 5,id);
    CreateBoard(2, 11, 6,id);
    CreateBoard(3, 10, 7,id);
    CreateBoard(4, 9, 8,id);
  }

  private void CreateBoard(int wherebeginX, int howmany, int wherebeginY, int id) //method to fill row of table with pawns
  {
    for (int i = wherebeginX; i <= (wherebeginX + howmany * 2 - 2); i = i + 2) {
      table[wherebeginY][i] = new Pawn(i, wherebeginY, 0);
      table[16 - wherebeginY][i] = new Pawn(i, 16 - wherebeginY, 0);
    }
  }
}
