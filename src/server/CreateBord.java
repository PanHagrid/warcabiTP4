package server;

import server.connection.Server;

public class CreateBord {
  private int pawnID, playerID;
  private static CreateBord INSTANCE;

  public static CreateBord getInstance(int maxPlayers) {
    if (INSTANCE == null)
      synchronized (CreateBord.class) {
        if (INSTANCE == null)
          INSTANCE = new CreateBord(maxPlayers);
      }
    return INSTANCE;
  }

  private CreateBord(int maxPlayers) {
    if (maxPlayers == 6) {
      playerID = 1;
      SetUpPlayer(1);
      playerID = 2;
      SetUpPlayer(2);
      playerID = 3;
      SetUpPlayer(3);
      playerID = 4;
      SetUpPlayer(4);
      playerID = 5;
      SetUpPlayer(5);
      playerID = 6;
      SetUpPlayer(6);
    } else if (maxPlayers == 3) {
      playerID = 1;
      SetUpPlayer(1);
      playerID = 2;
      SetUpPlayer(3);
      playerID = 3;
      SetUpPlayer(5);
    } else if (maxPlayers == 2) {
      playerID = 1;
      SetUpPlayer(1);
      playerID = 2;
      SetUpPlayer(4);
    } else if (maxPlayers == 4) {
      playerID = 1;
      SetUpPlayer(2);
      playerID = 2;
      SetUpPlayer(3);
      playerID = 3;
      SetUpPlayer(5);
      playerID = 4;
      SetUpPlayer(6);
    }
  }

  private void SetColors(int X, int Y, int coli, int howmany) {
    for (int i = X; i <= (X + howmany * 2 - 2); i = i + 2) {
      Server.board.table[Y][i].setcolour(playerID);
      Server.board.table[Y][i].setID(pawnID);

      Server.pawns[pawnID][1] = Y;
      Server.pawns[pawnID][0] = i;
      Server.pawns[pawnID][2] = playerID;
      Server.pawns[pawnID][3] = coli;
      pawnID++;
    }
  }

  private void SetUpPlayer(int number) {
    if (number == 1) { //north
      //player1
      SetColors(12, 0, 1, 1);
      SetColors(11, 1, 1, 2);
      SetColors(10, 2, 1, 3);
      SetColors(9, 3, 1, 4);
    } else if (number == 2) { //north-east
      //player2
      SetColors(18, 4, 2, 4);
      SetColors(19, 5, 2, 3);
      SetColors(20, 6, 2, 2);
      SetColors(21, 7, 2, 1);
    } else if (number == 3) {//south-east
      //player3
      SetColors(21, 9, 3, 1);
      SetColors(20, 10, 3, 2);
      SetColors(19, 11, 3, 3);
      SetColors(18, 12, 3, 4);
    } else if (number == 4) { //south
      //player4
      SetColors(12, 16, 4, 1);
      SetColors(11, 15, 4, 2);
      SetColors(10, 14, 4, 3);
      SetColors(9, 13, 4, 4);
    } else if (number == 5) {//south-west
      SetColors(3, 9, 5, 1);
      SetColors(2, 10, 5, 2);
      SetColors(1, 11, 5, 3);
      SetColors(0, 12, 5, 4);
    } else if (number == 6) { //north-wesr
      SetColors(3, 7, 6, 1);
      SetColors(2, 6, 6, 2);
      SetColors(1, 5, 6, 3);
      SetColors(0, 4, 6, 4);
    }
  }

  public static void movePawn(String buf1, String buf2, String buf3, String buf4, String buf5) {
    int playerID = Integer.parseInt(buf1);
    int newX = Integer.parseInt(buf4);
    int newY = Integer.parseInt(buf5);
    int oldX = Integer.parseInt(buf2);
    int oldY = Integer.parseInt(buf3);
    int pawnID = Server.board.table[oldY][oldX].getID();
    int color = Server.board.table[oldY][oldX].getcolour();
    Server.board.table[oldY][oldX].setcolour(0);
    Server.board.table[oldY][oldX].setID(-1);
    Server.board.table[newY][newX].setcolour(color);
    Server.board.table[newY][newX].setID(pawnID);

    Server.pawns[pawnID][1] = newY;
    Server.pawns[pawnID][0] = newX;
  }
}