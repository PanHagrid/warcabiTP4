package server.gui;

import server.connection.Server;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class QuantityPlayers extends JPanel implements ActionListener {
  public QuantityPlayers() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    //this.setLayout(new BorderLayout());
    JLabel text = new JLabel("<html>" + "Wybierz ilość graczy: 2, 3, 4 lub 6" + "</html>");
    text.setMaximumSize(new Dimension(Integer.MAX_VALUE, text.getMinimumSize().height));

    add(text);
    setLayout(new GridLayout(3, 0));
    String comboBoxItems[] = {"2", "3", "4", "6"};
    JComboBox cb = new JComboBox(comboBoxItems);

    add(cb);
    cb.addActionListener(this);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JComboBox cb = (JComboBox)e.getSource();
    Server.frame.x=Integer.parseInt((String)cb.getSelectedItem());
  }
}
