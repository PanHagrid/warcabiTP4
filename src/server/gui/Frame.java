package server.gui;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {
  QuantityPlayers quantity;
  public int x=2;//maxplayers
  public int y;//bots
  public Frame() {

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    setBounds(100, 100, 300, 300);
    setResizable(false);
    quantity = new QuantityPlayers();
    getContentPane().add(quantity, BorderLayout.NORTH);

    QuantityBots bots = new QuantityBots();
    getContentPane().add(bots, BorderLayout.CENTER);

    add (new Button(), BorderLayout.SOUTH);
  }
}
