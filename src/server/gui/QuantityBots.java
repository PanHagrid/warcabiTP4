package server.gui;

import server.connection.Server;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;


public class QuantityBots extends JPanel {
  public QuantityBots() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    //this.setLayout(new BorderLayout());
    JLabel text = new JLabel("<html>" + "Wybierz ilość botów - liczba musi być mniejsza od ilości graczy" + "</html>");
    text.setMaximumSize(new Dimension(Integer.MAX_VALUE, text.getMinimumSize().height));

    add(text);
    setLayout(new GridLayout(3, 0));

    final JSlider bots = new JSlider(JSlider.HORIZONTAL, 0, 5, 0);
    bots.setMinorTickSpacing(1);
    bots.setMajorTickSpacing(1);
    bots.setPaintTicks(true);
    bots.setPaintLabels(true);
    add(bots);


    bots.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        Server.frame.y = bots.getValue();
      }
    });
  }


}
