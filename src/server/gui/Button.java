package server.gui;


import server.connection.Server;

import javax.swing.*;
import java.awt.*;

class Button extends JButton {

  Button() {
    setText("Ok");
    setMaximumSize(new Dimension(Integer.MAX_VALUE, getMinimumSize().height));
    addActionListener(e -> {
      if (Server.frame.x>Server.frame.y) {
        Server.maxPlayers = Server.frame.x;
        Server.bots = Server.frame.y;
        Server.frame.setVisible(false);
        System.out.println("Serwer: Maksymalna ilość graczy: " + Server.frame.x);
        System.out.println("Serwer: Ilość botów: " + Server.frame.y);
      }else{
        setText("Za dużo botów");
      }
    });
  }
}