package server;

import board.Board;
import server.connection.Server;
import server.observer.Subject;

import java.io.BufferedReader;
import java.io.PrintStream;

public class CheckRules {

  public static void checkCommand(String buf, BufferedReader input, PrintStream output, Subject subject, boolean jump) {
    String[] bufs = buf.split(" ");
    switch (Integer.parseInt(bufs[0])) {
      case 1://move
        int oldX = Integer.parseInt(bufs[2]);
        int oldY = Integer.parseInt(bufs[3]);
        if (Server.jumpPawn != -1 && Server.jumpPawn != Server.board.table[oldY][oldX].getID()) {
          output.println("60");
        } else {
          if (!jump) {
            jump = CheckRules.checkJump(bufs[1], bufs[2], bufs[3], bufs[4], bufs[5]);
            if (jump) {sendMove(buf, subject);
            break;}
          }
          if (CheckRules.checkMove(bufs[1], bufs[2], bufs[3], bufs[4], bufs[5], jump)) {
            sendMove(buf, subject);
            if (!jump) Server.turn++;
            break;
          } else {
            output.println("60");
          }
        }
        break;
      case 2:
        Server.turn++;
        jump = false;
        Server.jumpPawn = -1;
        break;
      default:
        break;
    }
  }

  public static void sendMove(String buf, Subject subject) {
    String[] bufs = buf.split(" ");
    subject.setState("59 " + bufs[1] + " " + bufs[2] +
        " " + bufs[3] + " " + bufs[4] + " " + bufs[5]);
    CreateBord.movePawn(bufs[1], bufs[2], bufs[3], bufs[4], bufs[5]);
    int playerID = Integer.parseInt(bufs[1]);
    if (CheckRules.CheckWin(playerID, Server.board)) {
      Server.winList[playerID] = true;
      System.out.println("hahah");
      subject.setState("57 " + bufs[1]);
    }
  }


  public static Boolean checkMove(String buf1, String buf2, String buf3, String buf4, String buf5, Boolean jump) {
    int playerID = Integer.parseInt(buf1);
    int newX = Integer.parseInt(buf4);
    int newY = Integer.parseInt(buf5);
    int oldX = Integer.parseInt(buf2);
    int oldY = Integer.parseInt(buf3);

    if (newY > 16 || newX > 24 || newX < 0 || newY < 0) {
      return false;
    }
    if (Server.board.table[newY][newX] == null) {
      return false;
    }
    if (Server.board.table[oldY][oldX].getcolour() !=
        Server.pawns[playerID * 10 - 1][2]) {
      return false;
    }

    if (!jump) {
      if ((Server.board.table[newY][newX].getcolour() == 0)) {
        if ((oldX + 1 == newX) && (oldY - 1 == newY)) {//right upper
          return true;
        } else if ((oldX - 1 == newX) && (oldY - 1 == newY)) {//left upper
          return true;
        } else if ((oldX - 1 == newX) && (oldY + 1 == newY)) {//left down
          return true;
        } else if ((oldX + 1 == newX) && (oldY + 1 == newY)) {//right down
          return true;
        } else if ((oldX -2 == newX)&&(oldY==newY))
        {
          return true;
        }
        else if ((oldX +2 == newX)&&(oldY==newY))
        {
          return true;
        }
        else {
          return false;
        }
      } else {
        return checkJump(buf1, buf2, buf3, buf4, buf5);
      }
    }
    return false;
  }

  public static boolean checkJump(String buf1, String buf2, String buf3, String buf4, String buf5) {
    int playerID = Integer.parseInt(buf1);
    int newX = Integer.parseInt(buf4);
    int newY = Integer.parseInt(buf5);
    int oldX = Integer.parseInt(buf2);
    int oldY = Integer.parseInt(buf3);
    if (newY > 16 || newX > 24 || newX < 0 || newY < 0) {
      return false;
    }

    if (Server.board.table[oldY][oldX].getcolour() !=
        Server.pawns[playerID * 10 - 1][2]) {
      return false;
    }
    if (newY<0 ||newX<0){
      return false;
    }
    if (Server.board.table[newY][newX] == null) {
      return false;
    }
    if ((Server.board.table[newY][newX].getcolour() == 0)) {
      if ((oldX + 2 == newX) && (oldY - 2 == newY) && (Server.board.table[newY + 1][newX - 1].getcolour() != 0)) {//right-upper jump
        Server.jumpPawn = Server.board.table[oldY][oldX].getID();
        return true;
      } else if ((oldX + 2 == newX) && (oldY + 2 == newY) && (Server.board.table[newY - 1][newX - 1].getcolour() != 0)) {//right-down jump
        Server.jumpPawn = Server.board.table[oldY][oldX].getID();
        return true;
      } else if ((oldX - 2 == newX) && (oldY - 2 == newY) && (Server.board.table[newY + 1][newX + 1].getcolour() != 0)) {//left-upper jump
        Server.jumpPawn = Server.board.table[oldY][oldX].getID();
        return true;
      } else if ((oldX - 2 == newX) && (oldY + 2 == newY) && (Server.board.table[newY - 1][newX + 1].getcolour() != 0)) {//left-down jump
        Server.jumpPawn = Server.board.table[oldY][oldX].getID();
        return true;
      }
      else if ((oldY==newY)&&(oldX-4>0)&&(Server.board.table[oldY][oldX-2]!=null)&&(oldX -4 == newX))
      {
        if((Server.board.table[oldY][oldX-2].getcolour() != 0)) {
          Server.jumpPawn = Server.board.table[oldY][oldX].getID();
          return true;
        }
      }
      else if ((oldY==newY)&&(oldX +4 == newX))
      {
        if((Server.board.table[oldY][oldX+2].getcolour() != 0)) {
          Server.jumpPawn = Server.board.table[oldY][oldX].getID();
          return true;
        }
      }
    }
    return false;
  }

  public static boolean CheckWin(int ID, Board pattern) {
    int color=0;
    if(Server.maxPlayers==2)
    {
      if (ID==1)
      {
        color=1;
      }
      else if (ID==2)
      {
        color=4;
      }
    }
    else if(Server.maxPlayers==3) {
      if (ID == 1) {
        color = 1;
      } else if (ID == 2) {
        color = 3;
      } else {
        color = 5;
      }
    }
    else if(Server.maxPlayers==4) {
      if (ID == 1) {
        color = 2;
      } else if (ID == 2) {
        color = 3;
      } else if (ID==3) {
        color = 5;
      }
      else {color=6;}
    }
    else
    {
      color=ID;
    }




    if (color == 1) {
      return
          ((checkColour(12, 16, color, 1, pattern)) &&
              (checkColour(11, 15, color, 2, pattern)) &&
              (checkColour(10, 14, color, 3, pattern)) &&
              (checkColour(9, 13, color, 4, pattern)));
    } else if (color == 2) {
      return
          ((checkColour(3, 9, ID, 1, pattern)) &&
              (checkColour(2, 10, ID, 2, pattern)) &&
              (checkColour(1, 11, ID, 3, pattern)) &&
              (checkColour(0, 12, ID, 4, pattern)));
    } else if (color == 3) {
      return (
          (checkColour(3, 7, ID, 1, pattern)) &&
              (checkColour(2, 6, ID, 2, pattern)) &&
              (checkColour(1, 5, ID, 3, pattern)) &&
              (checkColour(0, 4, ID, 4, pattern)));
    } else if (color == 4) {
      return (
          (checkColour(12, 0, ID, 1, pattern)) &&
              (checkColour(11, 1, ID, 2, pattern)) &&
              (checkColour(10, 2, ID, 3, pattern)) &&
              (checkColour(9, 3, ID, 4, pattern)));
    } else if (color == 5) {
      return (
          (checkColour(18, 4, ID, 4, pattern)) &&
              (checkColour(19, 5, ID, 3, pattern)) &&
              (checkColour(20, 6, ID, 2, pattern)) &&
              (checkColour(21, 7, ID, 1, pattern))
      );
    } else
      return (checkColour(21, 9, ID, 1, pattern)) &&
          (checkColour(20, 10, ID, 2, pattern)) &&
          (checkColour(19, 11, ID, 3, pattern)) &&
          (checkColour(18, 12, ID, 4, pattern));
  }

  private static boolean checkColour(int X, int Y, int col, int howmany, Board pattern) {
    for (int i = X; i <= X + howmany * 2 - 2; i = i + 2) {
      if (pattern.table[Y][i].getcolour() != col) {
        return false;
      }
    }
    System.out.println("jest jeden " + col + howmany);
    return true;
  }

}
