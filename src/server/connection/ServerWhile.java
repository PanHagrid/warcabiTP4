package server.connection;

import server.CheckRules;
import server.observer.Observer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

import static java.lang.Thread.sleep;

public class ServerWhile extends Observer {
  private BufferedReader input;
  private PrintStream output;
  private int id;
  private Socket socket;

  private static boolean jump = false;

  ServerWhile(BufferedReader input, PrintStream output, int id, Socket socket) throws IOException, InterruptedException {
    this.input = input;
    this.output = output;
    this.id = id;
    this.socket = socket;
    String buf;
    this.subject = Server.subject;
    this.subject.attach(this);
    setStartInformation(input, output, id);

    subject.setState("62 " + id);
    subject.setState(Server.nicknames[id]);
    try {
      while (true) {
        buf = input.readLine();//catch init
        if (!checkBuf(buf)) return;

        if (Server.players == Server.maxPlayers - Server.bots) {
          if (id == Server.turn % Server.maxPlayers + 1) {
            if (Server.winList[id]) {
              System.out.println("test");
              Server.turn++;
              sleep(500);
            } else {
              output.println(64);
              while (buf.equals("inity")) buf = input.readLine();
              if (!checkBuf(buf)) return;
              CheckRules.checkCommand(buf, input, output, subject, jump);
            }
          } else {
            output.println(63 + " " + ((Server.turn % Server.maxPlayers) + 1));
            sleep(500);
          }
        } else {
          subject.setState("58");
          sleep(1000);
        }
      }
    } catch (Exception e) {
      System.out.println("[Gracz #" + id + " wychodzi z gry]");
      subject.setState("61 " + id);
      Server.usedName[id] = false;
      Server.players--;
      socket.close();
      e.printStackTrace();
    }
  }


  private void setStartInformation(BufferedReader input, PrintStream output, int id) {
    for (int i = 0; i < Server.maxPlayers; i++) {
      output.println(Server.nicknames[i]);
    }
    for (int i = 0; i < 10 * Server.maxPlayers; i++) {
      output.println(Server.pawns[i][0] + " " + Server.pawns[i][1] + " " + Server.pawns[i][2]);
    }
  }

  private Boolean checkBuf(String buf) throws IOException {
    if ((buf == null) || buf.equalsIgnoreCase("EXIT")) {
      System.out.println("[Gracz #" + id + " wychodzi z gry]");
      subject.setState("61 " + id);
      Server.usedName[id] = false;
      Server.players--;
      socket.close();
      return false;
    } else return true;
  }

  @Override
  public void update() {
    output.println(subject.getState());
  }

}
