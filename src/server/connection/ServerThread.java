package server.connection;

import java.io.*;
import java.net.Socket;
import java.util.Objects;

public class ServerThread extends Thread {
  protected Socket socket;

  public ServerThread(Socket clientSocket) {
    this.socket = clientSocket;
  }

  public void run() {
    InputStream inp = null;
    BufferedReader input = null;
    PrintStream output = null;
    String buf;
    int id = 0;
    try {
      inp = socket.getInputStream();
      input = new BufferedReader(new InputStreamReader(inp));
      output = new PrintStream(socket.getOutputStream());

      output.println(Server.maxPlayers);
      buf = input.readLine();
      for (int i = Server.bots; i <= Server.id; i++) {
        if (Objects.equals(Server.nicknames[i], buf)) {
          if (Server.usedName[i] == true) {
            output.println(Integer.toString(65));
            socket.close();
            return;
          }
          id = i;
          Server.usedName[id] = true;
          i = 10;
        }
      }
      if (id == 0) {
        if (Server.maxPlayers == Server.id) {
          output.println(Integer.toString(66));
          socket.close();
          return;
        }
        id = ++Server.id;
        Server.usedName[id] = true;
        Server.nicknames[id] = buf;
      }

      output.println(Integer.toString(id));
      System.out.println("[Gracz #" + id + " wchodzi do gry]");
      Server.players++;
      new ServerWhile(input, output, id, socket);

    } catch (IOException | NullPointerException e) {
      e.printStackTrace();
      return;
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}