package server.connection;

import board.Board;
import server.CreateBord;
import server.ai.BotNew;
import server.gui.Frame;
import server.observer.Subject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

import static java.lang.Thread.sleep;

public class Server {

  private static final int PORT = 6543;
  static int id = 0;
  static public String[] nicknames = new String[7];
  public static int maxPlayers = 0;
  public static int players = 0;
  static public boolean[] usedName = new boolean[7];
  static public Subject subject;
  public static int turn = 0;
  public static int bots = 0;
  //pawns[pawnID][x] - 0 is X; 1 is Y; 2 is color; 3 is playerID
  public static int pawns[][] = new int[60][4];
  public static Board board = null;

  static public Frame frame = new Frame();
  public static int jumpPawn = -1;
  public static boolean[] winList = new boolean[7];

  public static void main(String args[]) throws InterruptedException {
    frame.setVisible(true);
    board = new Board();
    while (true) {
      if (maxPlayers > 0) break;
      sleep(50);
    }
    //new CreatePawn(maxPlayers);
    for (int i=0;i<6;i++){
      nicknames[i]="";
    }
    if (bots > 0) new BotNew(bots);
    id = bots;
    CreateBord.getInstance(maxPlayers);
    turn= new Random().nextInt(maxPlayers)+1;
    startServer();

  }

  public static void startServer() {
    ServerSocket serverSocket = null;
    Socket socket = null;
    subject = new Subject();
    try {
      serverSocket = new ServerSocket(PORT);
      System.out.println("Serwer: Start na hoście-"
          + InetAddress.getLocalHost().getCanonicalHostName()
          + " port: " + serverSocket.getLocalPort());
    } catch (IOException e) {
      e.printStackTrace();
    }

    while (true) {
      try {
        socket = serverSocket.accept();
      } catch (IOException e) {
        e.printStackTrace();
      }

      new ServerThread(socket).start();
    }
  }
}
