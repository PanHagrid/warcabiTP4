package server.ai;

import server.connection.Server;
import server.observer.Observer;

public class BotWhile extends Observer {
  int id;
  static boolean jump;

  BotWhile(int id) throws InterruptedException {
    Thread.sleep(500);
    this.id = id;

    this.subject = Server.subject;

    subject.setState("62 " + id);
    subject.setState(Server.nicknames[id]);
    while (true) {
      Thread.sleep(500);
      if (Server.players == Server.maxPlayers - Server.bots) {
        if (id == Server.turn % Server.maxPlayers + 1) {
          BotMove g = new BotMove();
          g.doMove(id, Server.pawns);
        }
      }
    }
  }

  @Override
  public void update() {

  }
}
