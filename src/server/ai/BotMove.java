package server.ai;

import server.CheckRules;
import server.CreateBord;
import server.connection.Server;
import server.observer.Subject;

import static server.connection.Server.subject;

public class BotMove {
  public BotMove() {
  }


  public static boolean checkCommand(String buf, Subject subject) {
    String[] bufs = buf.split(" ");

    int oldX = Integer.parseInt(bufs[2]);
    int oldY = Integer.parseInt(bufs[3]);
    if (Server.jumpPawn != -1 &&
        Server.jumpPawn != Server.board.table[oldY][oldX].getID()) {
      return false;
    } else {
      if (!BotWhile.jump) {
        BotWhile.jump = CheckRules.checkJump(bufs[1], bufs[2], bufs[3], bufs[4], bufs[5]);
        if (BotWhile.jump) {
          sendMove(buf, subject);
          return true;
        }
      }
      if (CheckRules.checkMove(bufs[1], bufs[2], bufs[3], bufs[4], bufs[5], BotWhile.jump)) {
        sendMove(buf, subject);
        if (!BotWhile.jump) Server.turn++;
        return true;
      } else {
        return false;
      }
    }

  }


  private static void sendMove(String buf, Subject subject) {
    String[] bufs = buf.split(" ");
    subject.setState("59 " + bufs[1] + " " + bufs[2] +
        " " + bufs[3] + " " + bufs[4] + " " + bufs[5]);
    CreateBord.movePawn(bufs[1], bufs[2], bufs[3], bufs[4], bufs[5]);
    int playerID = Integer.parseInt(bufs[1]);
    if (CheckRules.CheckWin(playerID, Server.board)) {
      Server.winList[playerID] = true;
      subject.setState("57 " + bufs[1]);
    }
  }

  CheckRules checker = new CheckRules();

  public void doMove(int ID, int[][] pawns) {
    int color=0;
    if(Server.maxPlayers==2)
    {
      if (ID==1)
      {
        color=1;
      }
      else if (ID==2)
      {
        color=4;
      }
    }
    else if(Server.maxPlayers==3) {
      if (ID == 1) {
        color = 1;
      } else if (ID == 2) {
        color = 3;
      } else {
        color = 5;
      }
    }
    else if(Server.maxPlayers==4) {
      if (ID == 1) {
        color = 2;
      } else if (ID == 2) {
        color = 3;
      } else if (ID==3) {
        color = 5;
      }
      else {color=6;}
    }
    else
    {
      color=ID;
    }


    if (color == 1) {
      //skaczemy na ukos tak, zeby x pozostalo w przedziale [9,15]
      for (int i = 0; i < 10; i++) {
        if ((pawns[i][0] - 2) > 8) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1] + 2), subject)) {
            return;
          }
        }
      if ((pawns[i][0] + 2) < 16) {
        if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1] + 2), subject)) {
          return;
        }
      }
      }

      //rusz sie na ukos tak, zeby wspolrzedne pozostaly w przedziale [9..15]
      for (int i = 0; i < 10; i++) {
        if ((pawns[i][0] - 1) > 8) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 1) + " " + (pawns[i][1] + 1), subject)) {
            return;
          }
        }
        if ((pawns[i][0] + 1) < 16) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 1) + " " + (pawns[i][1] + 1), subject)) {
            return;
          }
        }
      }

      //rusz sie losowo na bok, tak, zeby wspolrzedne pozostaly w przedziale [9..15]
      for (int i = 0; i < 10; i++) {
        int tmp = (int) (Math.random() * 2 + 1);
        if ((tmp == 1) && (pawns[i][1]<13))
        {
        if(((pawns[i][0] - 2) > 8) &&
            (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1]), subject)))
          {
            return;
          }
          else if (((pawns[i][0] + 2) < 16) &&
          (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1]), subject))) {
          return;
        }
        } else if ((tmp == 2)&& (pawns[i][1]<13)) {
          if (((pawns[i][0] + 2) < 16) &&
              (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1]), subject))) {
            return;
          } else if (((pawns[i][0] - 1) > 8) &&
              (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1]), subject))) {
            return;
          }
        }
      }
      Server.turn++;
      BotWhile.jump = false;
      Server.jumpPawn = -1;
      return;


    } else if (color == 2) {
      //najpierw skaczemy w bok
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if ((pawns[i][0]-4>0)&&(pawns[i][0] - 4 + pawns[i][1] >= 12)) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 4) + " " + (pawns[i][1]), subject)) {
            return;
          }
        }
      }
      //potem skaczemy na ukos w prawo tak, zeby y<=12
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][1] + 2 <= 12) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1] + 2), subject)) {
            return;
          }
        }
      }
      //potem idziemy w bok lub skaczemy na ukos w prawo z zachowaniem warunkow wyzej
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if ((pawns[i][0]-4>0)&&(pawns[i][0] - 2 + pawns[i][1] >= 12)) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1]), subject)) {
            return;
          }
        } else if (pawns[i][1] + 1 <= 12) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 1) + " " + (pawns[i][1] + 1), subject)) {
            return;
          }
        }
      }
      //potem skaczemy na ukos w dol z warunkiem newx+newy<=18
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][0] + pawns[i][1] + 4 <= 18) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1] + 2), subject)) {
            return;
          }
        }
      }
      //potem idziemy na ukos z zachowaniem tego warunku
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][0] + pawns[i][1] + 2 <= 18) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 1) + " " + (pawns[i][1] + 1), subject)) {
            return;
          }
        }
      }
      //else skip tury
      Server.turn++;
      BotWhile.jump = false;
      Server.jumpPawn = -1;
      return;

    } else if (color == 3)
    {
      //nie potrafi jeszcze skonczyc
      //najpierw skaczemy w bok
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][0] - 4 >=7) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 4) + " " + (pawns[i][1]), subject)) {
            return;
          }
        }
      }
      //potem skaczemy na ukos w lewo tak, zeby y>=4
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][1] - 2 >=4) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1] - 2), subject)) {
            return;
          }
        }
      }
      //potem idziemy w bok lub skaczemy na ukos w lewo z zachowaniem warunkow wyzej
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if ((pawns[i][0] - 2 >=7)
          && (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " "
            + (pawns[i][1]), subject))) {
            return;
          }
         else if (pawns[i][1] - 1 >=4) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 1) + " " + (pawns[i][1] - 1), subject)) {
            return;
          }
        }
      }
      //potem skaczemy na ukos w dol z warunkiem y<=8
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][1] + 2 <= 8) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1] + 2), subject)) {
            return;
          }
        }
      }

      //potem skaczemy na ukos do gory w prawo
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if ((pawns[i][1] - 1 > 3)&&(pawns[i][0]+pawns[i][1]>11)) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 1) + " " + (pawns[i][1] - 1), subject)) {
            return;
          }
        }
      }
      //potem idziemy na ukos z zachowaniem tego warunku
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][1] + 1 <= 8) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 1) + " " + (pawns[i][1] + 1), subject)) {
            return;
          }
        }
      }
      //else skip tury
      Server.turn++;
      BotWhile.jump = false;
      Server.jumpPawn = -1;
      return;

    } else if (color == 4)
    {
      //skaczemy na ukos tak, zeby x pozostalo w przedziale [9,15]
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if ((pawns[i][0] - 2) > 8 && (pawns[i][0] + 2) < 16) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1] - 2), subject)) {
            return;
          } else if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1] - 2), subject)) {
            return;
          }
        }
      }

      //rusz sie na ukos tak, zeby wspolrzedne pozostaly w przedziale [9..15]
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if ((pawns[i][0] - 1) > 8 && (pawns[i][0] + 1) < 16) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 1) + " " + (pawns[i][1] - 1), subject)) {
            return;
          }
        }
        if ((pawns[i][0] - 1) > 8 && (pawns[i][0] + 1) < 16) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 1) + " " + (pawns[i][1] - 1), subject)) {
            return;
          }
        }
      }

      //rusz sie losowo na bok, tak, zeby wspolrzedne pozostaly w przedziale [9..15]
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        int tmp = (int) (Math.random() * 2 + 1);
        if ((tmp == 1) && ((pawns[i][0] - 2) > 8) && (pawns[i][1]>3) &&
            (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1]), subject))) {
          return;
        } else if ((tmp == 2) && ((pawns[i][0] + 2) < 16) && (pawns[i][1]>3) &&
            (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1]), subject))) {
          return;
        } else if (((pawns[i][0] - 1) > 8) && (pawns[i][1]>3) &&
            (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] - 2) + " " + (pawns[i][1]), subject))) {
          return;
        }
      }
      Server.turn++;
      BotWhile.jump = false;
      Server.jumpPawn = -1;
      return;
    }

     else if (color == 5)
    {
        //najpierw skaczemy w bok
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
          if (pawns[i][0] + 4 <= 17) {
            if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 4) + " " + (pawns[i][1]), subject)) {
              return;
            }
          }
        }
        //potem skaczemy na ukos w prawo
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
          if (pawns[i][1] - 2 >= 4) {
            if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1] - 2), subject)) {
              return;
            }
          }
        }
        //potem idziemy w bok lub skaczemy na ukos w prawo z zachowaniem warunkow wyzej
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
          if (pawns[i][0] + 2 <= 17) {
            if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1]), subject)) {
              return;
            }
          } else if ((pawns[i][1] - 1 >= 4)) {
            if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 1) + " " + (pawns[i][1] - 1), subject)) {
              return;
            }
          }
        }
        //potem skaczemy na ukos w dol z warunkiem y<=8
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
          if (pawns[i][1] + 2 <= 8) {
            if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1] + 2), subject)) {
              return;
            }
          }
        }
        //potem idziemy na ukos z zachowaniem tego warunku
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
          if (pawns[i][1] + 1 <= 8) {
            if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 1) + " " + (pawns[i][1] + 1), subject)) {
              return;
            }
          }
        }
        //else skip tury
        Server.turn++;
        BotWhile.jump = false;
        Server.jumpPawn = -1;
        return;

      } else if (color == 6)
    {
      //najpierw skaczemy w bok
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][0] + 4 <=17) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 4) + " " + (pawns[i][1]), subject)) {
            return;
          }
        }
      }
      //potem skaczemy na ukos w lewo tak, zeby y<=12
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][1] + 2 <= 12) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1] + 2), subject)) {
            return;
          }
        }
      }
      //potem idziemy w bok lub skaczemy na ukos w prawo z zachowaniem warunkow wyzej
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][0] + 4 <=17) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1]), subject)) {
            return;
          }
        } else if (pawns[i][1] + 1 <= 12) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 1) + " " + (pawns[i][1] + 1), subject)) {
            return;
          }
        }
      }
      //potem skaczemy na ukos w gore z warunkiem newy<=8
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][1] + 2 <= 8) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 2) + " " + (pawns[i][1] - 2), subject)) {
            return;
          }
        }
      }
      //potem idziemy na ukos z zachowaniem tego warunku
      for (int i = 10*(ID-1); i < 10*(ID-1)+10; i++) {
        if (pawns[i][1] + 2 <= 8) {
          if (checkCommand("1 " + ID + " " + pawns[i][0] + " " + pawns[i][1] + " " + (pawns[i][0] + 1) + " " + (pawns[i][1] - 1), subject)) {
            return;
          }
        }
      }
      //else skip tury
      Server.turn++;
      BotWhile.jump = false;
      Server.jumpPawn = -1;
      return;

    }

  }
}
