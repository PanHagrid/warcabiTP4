package server.ai;

public class BotNew {
  public BotNew(int bots) {
    Runnable[] runners = new Runnable[bots];
    Thread[] threads = new Thread[bots];

    for (int i = 0; i < bots; i++) {
      runners[i] = new BotThread(i+1);
    }

    for (int i = 0; i < bots; i++) {
      threads[i] = new Thread(runners[i]);
    }

    for (int i = 0; i < bots; i++) {
      threads[i].start();
    }
  }
}
