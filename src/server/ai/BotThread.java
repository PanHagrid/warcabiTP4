package server.ai;

import server.connection.Server;

public class BotThread implements Runnable {

  private int id;

  public BotThread(int id) {
    this.id = id;
  }

  @Override
  public void run() {
    try {
      Server.usedName[id] = true;
      Server.nicknames[id]=("Bot#"+id);
      new BotWhile(id);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}