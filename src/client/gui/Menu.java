package client.gui;

import client.connection.Client;

import javax.swing.*;
import java.awt.*;

public class Menu extends JFrame {
  private JDialog okienko;
  private JFrame nick;
  private JLabel text;
  private JTextField fild;
  private Menu g;

  public Menu() {
    super("Żydowskie warcaby");
    g = this;
    setLayout(new GridLayout(3,1));

    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(400, 300, 350, 250);
    setSize(351, 251);

    setVisible(true);
    // setLocationRelativeTo(null);

    JButton button = new JButton("Dołącz do gry");
    JButton button1 = new JButton("Info");
    JButton button2 = new JButton("Wyjdź :(");
    button.setAlignmentX(Component.CENTER_ALIGNMENT);
    button1.setAlignmentX(Component.CENTER_ALIGNMENT);
    button2.setAlignmentX(Component.CENTER_ALIGNMENT);

    add(button);
    add(button1);
    add(button2);

    button.addActionListener(e -> {
      g.setVisible(false);
      nick = new JFrame();
      nick.setTitle("Żydowski pseudonim");
      nick.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      nick.setBounds(400, 300, 250, 150);
      nick.setVisible(true);
      nick.setLayout(new GridLayout(3, 1));
      text = new JLabel("Podaj swoj pseudonim", JLabel.LEFT);
      nick.add(text);
      fild = new JTextField(20);
      nick.add(fild);
      JButton OK = new JButton("OK");
      nick.add(OK);
      OK.addActionListener(e1 -> {
        String nickname=fild.getText();
        if (nickname.equals("")){
          text.setText("Pseudonim nie może być pusty");
        }else {
          Client.nickname = nickname;
          nick.dispose();
          g.dispose();
        }

        //System.exit(0);
      });


    });
    button1.addActionListener(e -> {
      okienko = new JDialog();
      okienko.setTitle("Info");
      okienko.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
      okienko.setBounds(500, 300, 700, 300);
      okienko.setVisible(true);
      okienko.setLayout(new GridLayout(3, 1));
      text = new JLabel("Name: Jewish Checkers", JLabel.LEFT);
      okienko.add(text);
      text = new JLabel("Authors: Adam Howaniec and Filip Klich", JLabel.LEFT);
      okienko.add(text);
      text = new JLabel("Tutaj sie cos jeszcze wrzuci o zasadach", JLabel.LEFT);
      okienko.add(text);
    });
    button2.addActionListener(e -> System.exit(0));
  }
}
