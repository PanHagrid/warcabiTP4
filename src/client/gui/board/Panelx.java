package client.gui.board;

import client.HumanPlayer;
import client.connection.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class Panelx extends JPanel {
  private int players;
  private int count = 0;
  private double x, y, x1, y1;
  private HumanPlayer hp = new HumanPlayer();


  Panelx(int players) {
    this.players = players;
    setBackground(Color.white);
    setVisible(true);
    Dimension size = getPreferredSize();
    size.width = 750;
    size.height = 510;
    setPreferredSize(size);
    repaint();


    addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        if (Client.active) {
          count++;
          if (count == 1) {
            x = e.getX() / 30;
            y = e.getY() / 30;
            x1 = Math.floor(x);
            y1 = Math.floor(y);
            Client.board.table[0][0].setcolour(7);
            repaint();
          } else if (count == 2) {
            count = 0;
            x = e.getX();
            y = e.getY();
            y = y / 30;
            x = x / 30;
            x = Math.floor(x);
            y = Math.floor(y);
            Client.board.table[0][0].setcolour(0);
            repaint();

            if (Client.board.table[(int) y1][(int) x1] != null) {
              if (Client.board.table[(int) y1][(int) x1].getcolour() == Client.playersColor[Client.id]) {
                hp.move((int) x, (int) y, (int) x1, (int) y1, Client.board);
              }
            }
          }
        }
      }
    });
  }


  public void paint(Graphics g) {
    for (int i = 0; i < 25; i++) {
      for (int j = 0; j < 17; j++) {
        if (Client.board.table[j][i] != null) {
          setColour(Client.board.table[j][i].getcolour(), g);
          g.fillOval((Client.board.table[j][i].getCoordinateX() * 30), (Client.board.table[j][i].getCoordinateY() * 30), 30, 30);
        }
      }
    }
  }


  private void setColour(int number, Graphics gr) {
    if (number == 0) {
      gr.setColor(Color.black);
    } else if (number == 1) {
      gr.setColor(Color.blue);
    } else if (number == 2) {
      gr.setColor(Color.green);
    } else if (number == 3) {
      gr.setColor(Color.yellow);
    } else if (number == 4) {
      gr.setColor(Color.red);
    } else if (number == 5) {
      gr.setColor(Color.magenta);
    } else if (number == 6) {
      gr.setColor(Color.gray);
    } else if (number == 7) {
      gr.setColor(Color.pink);
    }
  }
}
