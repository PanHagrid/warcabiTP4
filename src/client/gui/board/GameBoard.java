package client.gui.board;

import client.connection.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GameBoard extends JFrame {
  public Panelx gamepanel;
  public Panely infopanel;

  public GameBoard(int players) {
    gamepanel = new Panelx(players);
    infopanel = new Panely();

    setVisible(true);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(1100, 570);
    //setLocationRelativeTo(null);
    setLayout(new FlowLayout(FlowLayout.LEFT));
    add(gamepanel);
    add(infopanel);
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e){
          Client.output.println();
      }
    });
  }
}
