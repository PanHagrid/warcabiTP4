package client.gui.board;

import client.connection.Client;

import javax.swing.*;
import java.awt.*;

public class Panely extends JPanel {

  JLabel[] serv = new ServerInfo[5];

  public String[] serverinfo = new String[5];
  JLabel[] players;

  Panely() {
    setVisible(true);
    Dimension size = getPreferredSize();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    double x = screenSize.getWidth()-850;

    System.out.println(x);
    if (x>250) x=250;
    System.out.println(x);
    size.width = (int)x;
    size.height = 510;
    setPreferredSize(size);
    setLayout(new GridLayout(6 + Client.maxPlayers, 1));

    for (int i = 0; i < 5; i++) {
      serv[i] = new ServerInfo();
      add(serv[i]);
    }

    players = new JLabel[Client.maxPlayers];
    for (int i = 0; i < Client.maxPlayers; i++) {
      players[i] = new Players();
      add(players[i]);
      players[i].setText((i + 1) + ". ");
    }

    add(new Skip());
  }

  public void newServerInfo(String info) {
    for (int i = 0; i < 4; i++) {
      serverinfo[i] = serverinfo[i + 1];
      serv[i].setText(serverinfo[i]);
    }
    serverinfo[4] = info;
    serv[4].setText(info);
  }

  public void changePlayerName(String info, int id) {
    //players[id].setText("<html><font color='red'>" + (id + 1) + ". " + info + "</font></html>");
    switch (Client.playersColor[id]) {
      case 0:
        players[id].setText("<html><font color='blue'>" + (id + 1) + ". " + info + "</font></html>");
        break;
      case 1:
        players[id].setText("<html><font color='green'>" + (id + 1) + ". " + info + "</font></html>");
        break;
      case 2:
        players[id].setText("<html><font color='yellow'>" + (id + 1) + ". " + info + "</font></html>");
        break;
      case 3:
        players[id].setText("<html><font color='red'>" + (id + 1) + ". " + info + "</font></html>");
        break;
      case 4:
        players[id].setText("<html><font color='magenta'>" + (id + 1) + ". " + info + "</font></html>");
        break;
      case 5:
        players[id].setText("<html><font color='gray'>" + (id + 1) + ". " + info + "</font></html>");
        break;
    }
  }

  public class ServerInfo extends JLabel {
    ServerInfo() {
      setText("");
      setFont(new Font("Serif", Font.PLAIN, 14));
    }
  }

  public class Players extends JLabel {
    Players() {
      setFont(new Font("Serif", Font.PLAIN, 14));
    }
  }


  class Skip extends JButton {
    Skip() {
      setText("Zakończ turę");
      setMaximumSize(new Dimension(Integer.MAX_VALUE, getMinimumSize().height));
      addActionListener(e -> {
        if (Client.active) {
          Client.active = false;
          Client.output.println("2");
          Client.jump = false;
        }
      });
    }
  }
}

