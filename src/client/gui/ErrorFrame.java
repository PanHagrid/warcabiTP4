package client.gui;

import javax.swing.*;
import java.awt.*;

public class ErrorFrame extends JFrame {
  public ErrorFrame(String err) {
    setTitle("Błąd");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(400, 300, 250, 150);

    setVisible(true);

    setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
    JLabel label = new JLabel(err);
    OK button = new OK();
    label.setAlignmentX(Component.CENTER_ALIGNMENT);
    button.setAlignmentX(Component.CENTER_ALIGNMENT);

    add(label);
    add(button);
  }

  class OK extends JButton {
    OK() {
      setText("Ok");
      addActionListener(e -> {
        System.exit(0);
      });
    }
  }
}
