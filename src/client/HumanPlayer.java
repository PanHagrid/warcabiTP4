package client;


import board.Board;
import client.connection.Client;

import static client.connection.Client.guiBoard;

public class HumanPlayer {
  //method to move Pawn(returns true if move has succed, false if it failed to move):
  public void move(int newX, int newY, int oldX, int oldY, Board bord) { //poki co nie sprawdzamy czy podany pion nalezy do tego gracza
    if (checkmove(newX, newY, oldX, oldY, bord)) {
      Client.output.println("1 " + Client.id + " " + oldX + " " + oldY + " " + newX + " " + newY);
      Client.active = false;
    }else{
      Client.guiBoard.infopanel.newServerInfo("K: Ruch niemożliwy");
    }
  }

  public static void makeMoveOnBoard(int oldX, int oldY, int newX, int newY, int kolor) {
    Client.board.table[oldY][oldX].setcolour(0);
    Client.board.table[newY][newX].setcolour(kolor);
    int id = Client.board.table[oldY][oldX].getID();
    Client.board.table[oldY][oldX].setID(-1);
    Client.board.table[newY][newX].setID(id);
    guiBoard.gamepanel.repaint();
  }

  private boolean checkmove(int newX, int newY, int oldX, int oldY, Board bord) {
    if (Client.jumpPawn != Client.board.table[oldY][oldX].getID() && Client.jump) {
      return false;
    }
    if ((bord.table[newY][newX] != null) && (bord.table[newY][newX].getcolour() == 0)) {
      if ((oldX + 1 == newX) && (oldY - 1 == newY) && !Client.jump) {//right upper
        return true;
      } else if ((oldX - 1 == newX) && (oldY - 1 == newY) && !Client.jump) {//left upper
        return true;
      } else if ((oldX - 1 == newX) && (oldY + 1 == newY) && !Client.jump) {//left down
        return true;
      } else if ((oldX + 1 == newX) && (oldY + 1 == newY && !Client.jump)) {//right down
        return true;
      } else if ((oldX + 2 == newX) && (oldY == newY) && !Client.jump) { //right
        return true;
      } else if ((oldX - 2 == newX) && (oldY == newY) && !Client.jump) {//left
        return true;
      } else if ((oldX == newX) && (oldY - 2 == newY) && !Client.jump) {//up
        return true;
      } else if ((oldX == newX) && (oldY + 2 == newY) && !Client.jump) {//down
        return true;
      } else if ((oldX == newX) && (oldY - 4 == newY) && (bord.table[newY + 2][newX].getcolour() != 0)) { //up jump
        Client.jumpPawn = Client.board.table[oldY][oldX].getID();
        Client.jump = true;
        return true;
      } else if ((oldX == newX) && (oldY + 4 == newY) && (bord.table[newY - 2][newX].getcolour() != 0)) {//down jump
        Client.jumpPawn = Client.board.table[oldY][oldX].getID();
        Client.jump = true;
        return true;
      } else if ((oldX - 4 == newX) && (oldY == newY) && (bord.table[newY][newX + 2].getcolour() != 0)) {//left jump
        Client.jump = true;
        Client.jumpPawn = Client.board.table[oldY][oldX].getID();
        return true;
      } else if ((oldX + 4 == newX) && (oldY == newY) && (bord.table[newY][newX - 2].getcolour() != 0)) {//right jump
        Client.jumpPawn = Client.board.table[oldY][oldX].getID();
        Client.jump = true;
        return true;
      } else if ((oldX + 2 == newX) && (oldY - 2 == newY) && (bord.table[newY + 1][newX - 1].getcolour() != 0)) {//right-upper jump
        Client.jump = true;
        Client.jumpPawn = Client.board.table[oldY][oldX].getID();
        return true;
      } else if ((oldX + 2 == newX) && (oldY + 2 == newY) && (bord.table[newY - 1][newX - 1].getcolour() != 0)) {//right-down jump
        Client.jump = true;
        Client.jumpPawn = Client.board.table[oldY][oldX].getID();
        return true;
      } else if ((oldX - 2 == newX) && (oldY - 2 == newY) && (bord.table[newY + 1][newX + 1].getcolour() != 0)) {//left-upper jump
        Client.jump = true;
        Client.jumpPawn = Client.board.table[oldY][oldX].getID();
        return true;
      } else if ((oldX - 2 == newX) && (oldY + 2 == newY) && (bord.table[newY - 1][newX + 1].getcolour() != 0)) {//left-down jump
        Client.jump = true;
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public void ShowAvaiableMoves() //to work on
  {

  }
}

