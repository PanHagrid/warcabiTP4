package client.connection;

import board.Board;
import client.gui.ErrorFrame;
import client.gui.Menu;
import client.gui.board.GameBoard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.Socket;

import static java.lang.Thread.sleep;

public class Client {
  public static int id = 0;
  public static String nickname;
  static String[] nicknames = new String[7];
  static BufferedReader input = null;
  public static boolean active=false;
  public static boolean jump=false;
  public static PrintStream output = null;
  public static int maxPlayers;
  static Socket skt;
  public static GameBoard guiBoard;
  public static Board board;
  public static int[] playersColor=new int[7];
  public static int jumpPawn;
  public static int []winners =new int[7];
  public static int win=0;

  public static void main(String args[]) throws IOException {
    new Menu();
    board=new Board();
    while (true) {
      try {
        sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      if (nickname != null) {
        break;
      }
    }

    try {
      new ClientConnect(nickname);
      guiBoard = new GameBoard(maxPlayers);
      new ClientWhile();
    } catch (ConnectException e) {
      e.printStackTrace();
      new ErrorFrame("Serwera nie znaleziono");
    } catch (IOException | NullPointerException |InterruptedException e) {
      e.printStackTrace();
      new ErrorFrame("Utracono połączenie z serwerem");
      skt.close();
      guiBoard.dispose();
    }
  }
}
