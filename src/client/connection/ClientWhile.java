package client.connection;

import client.HumanPlayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import static client.connection.Client.nicknames;
import static client.connection.Client.winners;
import static java.lang.Thread.sleep;

class ClientWhile {
  public static String otherRound;
  public static boolean failRound = false;

  ClientWhile() throws IOException, InterruptedException {
    otherRound = "";

    for (int i = 0; i < Client.maxPlayers; i++)
      Client.guiBoard.infopanel.changePlayerName(Client.nicknames[i+1], i);

    while (true) {
      if (Client.active) {
        sleep(50);
      } else {
        Client.output.println("inity");//nothing interested, really dude
        checkCommand(Client.input, Client.output, Client.id, Client.maxPlayers);
      }
    }
  }

  private void checkCommand(BufferedReader input, PrintStream output, int id, int maxPlayers)
      throws InterruptedException, IOException {
    String buf = input.readLine();
    if (buf != null) {
      String[] bufs = buf.split(" ");
      switch (Integer.parseInt(bufs[0])) {
        case 57:
          int player = Integer.parseInt(bufs[1]);
          Client.guiBoard.infopanel.newServerInfo("S: Uhuhu nowa wygrana " + nicknames[player]);
          winners[Client.win++] = player;
          break;
        case 58:
          Client.guiBoard.infopanel.newServerInfo("S: Oczekiwanie na graczy");
          break;
        case 59:
          Client.active = false;
          int playerID = Integer.parseInt(bufs[1]);
          int oldX = Integer.parseInt(bufs[2]);
          int oldY = Integer.parseInt(bufs[3]);
          int newX = Integer.parseInt(bufs[4]);
          int newY = Integer.parseInt(bufs[5]);
          HumanPlayer.makeMoveOnBoard(oldX, oldY, newX, newY, Client.playersColor[playerID]);
          break;
        case 62://joined the game
          buf = input.readLine();
          nicknames[Integer.parseInt(bufs[1])] = buf;
          Client.guiBoard.infopanel.newServerInfo("#" + bufs[1] + " " + buf + " wchodzi do gry");
          Client.guiBoard.infopanel.changePlayerName(Client.nicknames[Integer.parseInt(bufs[1])], Integer.parseInt(bufs[1])-1);
          break;
        case 61://leaved the game
          Client.guiBoard.infopanel.newServerInfo("#" + bufs[1] + " " + nicknames[Integer.parseInt(bufs[1])] + " wychodzi z gry");
          break;
        case 63:
          if (!ClientWhile.otherRound.equals(bufs[1])) {
            Client.guiBoard.infopanel.newServerInfo("S: Tura gracza #" + bufs[1]);
          }
          ClientWhile.otherRound = bufs[1];
          break;
        case 60:
          ClientWhile.failRound = true;
          Client.guiBoard.infopanel.newServerInfo("S: Ruch niemożliwy");
          Client.active = false;
          break;
        case 64:
          if (!ClientWhile.failRound)
            Client.guiBoard.infopanel.newServerInfo("S: Tura gracza #" + id + " czyli Twoja");
          ClientWhile.failRound = false;
          ClientWhile.otherRound = "";
          Client.active = true;
//          while (true) {
//            buf = input.readLine();
//            bufs = buf.split(" ");
//
//            if (buf.equals("60")) {
//              System.out.println("Serwer: Ruch niemożliwy");
//            } else {
//              System.out.println("TEST "+buf);
//              break;
//            }
//          }
          break;
        default:
          break;
      }
    } else {
      throw new InterruptedException("");
    }
  }


}
