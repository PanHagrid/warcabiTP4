package client.connection;

import board.Pawn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

class ClientConnect {
  private static final int port = 6543;

  ClientConnect(String nickname) throws IOException {
    String host;
    host = "localhost";
    System.out.println("Klient: Próba podłączenia do serwera  port: " + port + '.');
    Client.skt = new Socket(host, port);
    //Client.skt.setSoTimeout(1000);
    Client.input = new BufferedReader(new InputStreamReader(Client.skt.getInputStream()));
    Client.output = new PrintStream(Client.skt.getOutputStream());

    Client.maxPlayers = Integer.parseInt(Client.input.readLine());
    System.out.println("Serwer: Maksymalna ilość graczy: " + Client.maxPlayers);
    Client.output.println(nickname);
    Client.id = Integer.parseInt(Client.input.readLine());
    if (Client.id == 66) {
      System.out.println("Serwer: Osiągnięto maksymalną liczbę graczy");
      return;
    } else if (Client.id == 65) {
      System.out.println("Serwer: Gracz " + nickname + " jest na serwerze");
      return;
    }
    System.out.println("Klient: Gracz #" + Client.id + "\n" + "Klient: Pseudonim: " + nickname);
    setStartInformation(Client.input, Client.output, Client.id, Client.maxPlayers);
  }

  private void setStartInformation(BufferedReader input, PrintStream output, int id, int maxPlayers) throws IOException {
    for (int i = 0; i < maxPlayers; i++) {
      Client.nicknames[i] = input.readLine();
    }
    String buf;
    String[] bufs;
    int playerID, newX, newY, color;
    for (int i = 0; i < 10 * maxPlayers; i++) {
      buf = input.readLine();
      bufs = buf.split(" ");
      playerID = (i / 10 + 1);
      newX = Integer.parseInt(bufs[0]);
      newY = Integer.parseInt(bufs[1]);
      color = Integer.parseInt(bufs[2]);
      Client.playersColor[playerID] = color;
      Client.board.table[newY][newX] = new Pawn(newX, newY, color);
    }
  }
}
